#include <ESP8266WiFi.h>
#include <IRremoteESP8266.h>
#include <ESP8266WebServer.h>
#include "remote_definitions.h"

const char* ssid = _SSID_;
const char* password = _WIFI_PASSWORD_;
const int led = 13;

ESP8266WebServer server(80);
IRsend irsend(4);
IRsend *irptr = &irsend;

void callCommand(String deviceName, String command) {
	make_call(irptr, deviceName, command);
	server.send(200, "text/plain", "Success");
}

void handleControlRequest() {
	Serial.println("In handler");
	digitalWrite(led, 1);

	if (!((server.hasArg("device")) && (server.hasArg("device")))) {
		server.send(404, "text/plain", "Needs args");
	}

    String device = server.arg("device");
    String command = server.arg("command");
	Serial.println(device == command);
    callCommand(device, command);
}


void handleNotFound() {
	digitalWrite ( led, 1 );
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for ( uint8_t i = 0; i < server.args(); i++ ) {
		message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
	}
	Serial.println(message);
	server.send ( 404, "text/plain", message );
	digitalWrite ( led, 0 );
}

void setup()
{
  Serial.begin(9600);

  WiFi.begin(ssid, password);

  Serial.println("\n");
  Serial.print("Connecting");
  Serial.println("");
  Serial.println(ssid);
  Serial.println(password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  server.on("/control", handleControlRequest);
  server.onNotFound(handleNotFound);
  Serial.println("Handlers attached");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());

  irsend.begin();
  Serial.println("IR started");
  //irsend.sendRC6(philips_tv.vol_down, philips_tv.bits);
}

void loop() {
  server.handleClient();
}
