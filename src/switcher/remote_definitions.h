#ifndef REMOTE_DEFINITIONS_H
#define REMOTE_DEFINITIONS_H

#include <IRremoteESP8266.h>
#include <WString.h>

struct button {
  String key;
  unsigned long code;
};

struct remote_definition {
  String key;
  button buttons[30];
  void (*fn)(IRsend *_irsend, unsigned long code);
};

extern remote_definition remotes[3];

bool make_call(IRsend *_irsend, String remote_name, String key);

#endif
