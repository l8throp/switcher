#include "remote_definitions.h"
#include <string.h>
#include <WString.h>
#include <HardwareSerial.h>

void tv_send(IRsend *_irsend, unsigned long data) {
    _irsend->sendRC6(data, 22);
};

void dvr_send(IRsend *_irsend, unsigned long data) {
    _irsend->sendTWC(data);
};

void null_send(IRsend *_irsend, unsigned long data) {};

remote_definition remotes[3] = {
		{"tv", {{ "bits",       20 },
				{ "power",      0x1000C },
				{ "volUp",      0x10010 },
				{ "volDown",    0x10011 },
				{ "mute",       0x10038 },
				{ "input",      0x10038 },
				{ "null",       0 }},
		 &tv_send},

		{"dvr", {{ "bits",        22 },
				 { "power",       0x37C107 },
				 { "chanUp",      0x377111 },
				 { "chanDown",    0x36F121 },
				 { "btn1",        0x36113D },
				 { "btn2",        0x37111D },
				 { "btn3",        0x36912D },
				 { "btn4",        0x37910D },
				 { "btn5",        0x365135 },
				 { "btn6",        0x375115 },
				 { "btn7",        0x36D125 },
				 { "btn8",        0x37D105 },
				 { "btn9",        0x363139 },
				 { "btn0",        0x373119 },
				 { "up",          0x36812F },
				 { "down",        0x37A10B },
				 { "left",        0x37810F },
				 { "right",       0x364137 },
				 { "select",      0x366133 },
				 { "info",        0x36C127 },
				 { "exit",        0x366932 },
				 { "guide",       0x36C127 },
				 { "last",        0x36E123 },
				 { "pause",       0x374117 },
				 { "play",        0x37990C },
				 { "rewind",      0x37291A },
				 { "fastForward", 0x36293A},
				 { "stop",        0x365934 },
				 { "null",        0 }},
		 &dvr_send},

		{"null", {{ "null", 0 }}, &null_send}
};

bool _make_call(remote_definition remote, IRsend *_irsend, String key){
	for (int i; remote.buttons[i].key != "null"; i++) {
		Serial.println(remote.buttons[i].key);
		if (remote.buttons[i].key == key) {
			Serial.println(remote.buttons[i].code);
			(*(remote.fn))(_irsend, remote.buttons[i].code);
			return true;
		}
	}
    return false;
}

bool make_call(IRsend *_irsend, String remote_name, String key){
	Serial.println(remote_name);
	Serial.print("one key: ");
	Serial.println(remotes[1].key);
	for (int i; remotes[i].key != "null"; i++) {
		Serial.println(remotes[i].key);
		if (remotes[i].key == remote_name) {
			return _make_call(remotes[i], _irsend, key);
		}
	}
    return false;
}
